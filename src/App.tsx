import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from './store'
import { Layout } from './components/layout/Layout';
import { GiftStepOne } from './form/formstep1';
import { GiftStepTwo } from './form/formstep2';
import { Product } from './form/product';
import { FormLoader } from './form/formloader';
import { useEffect } from 'react';
import { FormProps, handleFormValues } from './form/formSlice';

function App() {
  const  { form } = useSelector((state: RootState) => state);
  const dispatch = useDispatch<AppDispatch>();
  const { currentStep } = form;
  useEffect(()=>{
    if(currentStep === 'progress') {
      const val:FormProps = {
        currentStep: 'Finished'
      }
      setTimeout(() => {
        dispatch(handleFormValues(val))
      },2000)
    }
  },[currentStep])
  return (
    <Layout>
      {
        currentStep === 1 && <GiftStepOne />
      }
      {
        currentStep === 2 && <GiftStepTwo />
      }
      {
        currentStep == 'progress' && <FormLoader />
      }
      {
        currentStep === 'Finished' && <Product />
      }
    </Layout>
  );
}

export default App;
