import styled from "styled-components"
import RightArrow from "../assets/images/arrow right.png";
import productSideLogo from '../assets/images/productsidelogo.png';
import PlusIcon from '../assets/images/plus-circle.png';
import Prod1 from '../assets/images/p1.png'
import { colors } from "../assets/colors";


export const Product: React.FC = () => {
    return (
        <ProductWrapper>
            <ProductContainer>
                <HeaderTextContainer>
                    <TittleText>
                        Genie:
                    </TittleText>
                    <h1>AI Gift Ideas</h1>
                </HeaderTextContainer>

                <TextBar>
                    <TextBarContent>
                        Recommmend birthday gift ideas for my 55 year old mother.She likes pickleball,ge
                    </TextBarContent>
                    <TextBarIcon>
                        <img src={productSideLogo} />
                    </TextBarIcon>
                </TextBar>
                 <div style={{ display: 'flex' }}>
                    <NormalTxt>Not liking these suggestions?
                    Try again with more details or get help from Genine
                    </NormalTxt>
                    {/* <NormalTxt color={colors.primary}> get help from Genine</NormalTxt> */}
                </div>
                <CardContainer>
                    <CardTopBar>
                        <h1>Beauty gift basket</h1>
                        <SeeMore>
                            See more
                            <img src={RightArrow} />
                        </SeeMore>
                    </CardTopBar>
                    <CardBoxInnerContainer>
                        <CardBox>
                            <CardBoxImgContainer>
                                <CardImageWrapper>
                                    <img src={Prod1} />
                                </CardImageWrapper>
                                <CardBoxImgIcon>

                                <img src={PlusIcon} />
                                </CardBoxImgIcon>
                            </CardBoxImgContainer>
                            <NormalTxt color={colors.primary}> 1000 and 1 flowers</NormalTxt>
                            <NormalTxt color={colors.black} isBold={600}>Beautiful Bouquet</NormalTxt>
                            <NormalTxt>$530</NormalTxt>
                            <NormalTxt fontSize={0.9}>Typical range: $100-$600</NormalTxt>
                        </CardBox>
                        <CardBox>
                            <CardBoxImgContainer>
                                <CardImageWrapper>
                                    <img src={Prod1} />
                                </CardImageWrapper>
                                <CardBoxImgIcon>

                                <img src={PlusIcon} />
                                </CardBoxImgIcon>
                            </CardBoxImgContainer>
                            <NormalTxt color={colors.primary}> 1000 and 1 flowers</NormalTxt>
                            <NormalTxt color={colors.black} isBold={600}>Beautiful Bouquet</NormalTxt>
                            <NormalTxt>$530</NormalTxt>
                            <NormalTxt fontSize={0.9}>Typical range: $100-$600</NormalTxt>
                        </CardBox>
                        <CardBox>
                            <CardBoxImgContainer>
                                <CardImageWrapper>
                                    <img src={Prod1} />
                                </CardImageWrapper>
                                <CardBoxImgIcon>

                                <img src={PlusIcon} />
                                </CardBoxImgIcon>
                            </CardBoxImgContainer>
                            <NormalTxt color={colors.primary}> 1000 and 1 flowers</NormalTxt>
                            <NormalTxt color={colors.black} isBold={600}>Beautiful Bouquet</NormalTxt>
                            <NormalTxt>$530</NormalTxt>
                            <NormalTxt fontSize={0.9}>Typical range: $100-$600</NormalTxt>
                        </CardBox>
                        <CardBox>
                            <CardBoxImgContainer>
                                <CardImageWrapper>
                                    <img src={Prod1} />
                                </CardImageWrapper>
                                <CardBoxImgIcon>

                                <img src={PlusIcon} />
                                </CardBoxImgIcon>
                            </CardBoxImgContainer>
                            <NormalTxt color={colors.primary}> 1000 and 1 flowers</NormalTxt>
                            <NormalTxt color={colors.black} isBold={600}>Beautiful Bouquet</NormalTxt>
                            <NormalTxt>$530</NormalTxt>
                            <NormalTxt fontSize={0.9}>Typical range: $100-$600</NormalTxt>
                        </CardBox>
                    </CardBoxInnerContainer>
                </CardContainer>
                <CardContainer>
                    <CardTopBar>
                        <h1>Beauty gift basket</h1>
                        <SeeMore>
                            See more
                            <img src={RightArrow} />
                        </SeeMore>
                    </CardTopBar>
                    <CardBoxInnerContainer>
                        <CardBox>
                            <CardBoxImgContainer>
                                <CardImageWrapper>
                                    <img src={Prod1} />
                                </CardImageWrapper>
                                <CardBoxImgIcon>

                                <img src={PlusIcon} />
                                </CardBoxImgIcon>
                            </CardBoxImgContainer>
                            <NormalTxt color={colors.primary}> 1000 and 1 flowers</NormalTxt>
                            <NormalTxt color={colors.black} isBold={600}>Beautiful Bouquet</NormalTxt>
                            <NormalTxt>$530</NormalTxt>
                            <NormalTxt fontSize={0.9}>Typical range: $100-$600</NormalTxt>
                        </CardBox>
                        <CardBox>
                            <CardBoxImgContainer>
                                <CardImageWrapper>
                                    <img src={Prod1} />
                                </CardImageWrapper>
                                <CardBoxImgIcon>

                                <img src={PlusIcon} />
                                </CardBoxImgIcon>
                            </CardBoxImgContainer>
                            <NormalTxt color={colors.primary}> 1000 and 1 flowers</NormalTxt>
                            <NormalTxt color={colors.black} isBold={600}>Beautiful Bouquet</NormalTxt>
                            <NormalTxt>$530</NormalTxt>
                            <NormalTxt fontSize={0.9}>Typical range: $100-$600</NormalTxt>
                        </CardBox>
                        <CardBox>
                            <CardBoxImgContainer>
                                <CardImageWrapper>
                                    <img src={Prod1} />
                                </CardImageWrapper>
                                <CardBoxImgIcon>

                                <img src={PlusIcon} />
                                </CardBoxImgIcon>
                            </CardBoxImgContainer>
                            <NormalTxt color={colors.primary}> 1000 and 1 flowers</NormalTxt>
                            <NormalTxt color={colors.black} isBold={600}>Beautiful Bouquet</NormalTxt>
                            <NormalTxt>$530</NormalTxt>
                            <NormalTxt fontSize={0.9}>Typical range: $100-$600</NormalTxt>
                        </CardBox>
                        <CardBox>
                            <CardBoxImgContainer>
                                <CardImageWrapper>
                                    <img src={Prod1} />
                                </CardImageWrapper>
                                <CardBoxImgIcon>

                                <img src={PlusIcon} />
                                </CardBoxImgIcon>
                            </CardBoxImgContainer>
                            <NormalTxt color={colors.primary}> 1000 and 1 flowers</NormalTxt>
                            <NormalTxt color={colors.black} isBold={600}>Beautiful Bouquet</NormalTxt>
                            <NormalTxt>$530</NormalTxt>
                            <NormalTxt fontSize={0.9}>Typical range: $100-$600</NormalTxt>
                        </CardBox>
                    </CardBoxInnerContainer>
                </CardContainer>
            </ProductContainer>
        </ProductWrapper>
    )
}

interface textProps {
    color?: string,
    isBold?: number,
    fontSize?: number,
}
export const ProductWrapper = styled.div`
    width:100%;
    height:auto;
    display:flex;
    justify-content:center;
`
export const ProductContainer = styled.div`
    width:70%;
    height:auto;
    margin-top:60px;
`
const HeaderTextContainer = styled.div`
    display:flex;
    gap:10px;
`
const TittleText = styled.h1`
    background: linear-gradient(90deg, rgba(80,188,217,1) 31%, rgba(241,77,255,1) 71%);
    background: -webkit-linear-gradient(90deg, rgba(80,188,217,1) 31%, rgba(241,77,255,1) 71%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
`
const TextBar = styled.div`
    height:auto;
    width:100%;
    padding:5px 10px;
    border:2px solid ${colors.grey};
    border-radius:5px;
    display:flex;
   justify-content:space-around;
   margin:20px 0px;
`
const TextBarContent = styled.div`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width:90%;
    font-size:1.6rem;
    display:flex;
    align-items:center;
`
const TextBarIcon = styled.div`
    width:10%;
    height:10%;
    display:flex;
    align-items:center;
    justify-content:center;
    img {
        
    }
`
const NormalTxt = styled.div<textProps>`
    color:${({ color }) => color ? color : '#000'};
    font-size:${({ fontSize }) => fontSize ? fontSize : '1.1'}rem;
    font-weight:${({ isBold }) => isBold ? isBold : '400'};
    margin:0px 1px;
`

const CardContainer = styled.div`
    width:100%;
    height:auto;
    margin:60px 0px;
`
const CardTopBar = styled.div`
    display:flex;
    justify-content:space-between;
    align-items:center;
`
const SeeMore = styled.div`
    color:${colors.primary};
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 5px;
    img {
        background-size:cover;
        margin-top:5px;
    }
`
const CardBoxInnerContainer = styled.div`
    display:flex;
    margin-bottom:20px;
    height:auto;
    flex-wrap:wrap;
    justify-content:space-between;
`
const CardBox = styled.div`
    height:100%;
    padding:10px 20px;
    margin:0px 10px;
    display:flex;
    flex-direction:column;
    gap:6px;
    border:1px solid ${colors.white};
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
`
const CardBoxImgContainer = styled.div`
    display:flex;
    gap:10px;
`
const CardBoxImgIcon = styled.div`
    height:30px;
    width:30px;
    img {
        background-size:cover;
    }
`
const CardImageWrapper = styled.div`
    display:flex;
    justify-content:center;
    align-items:center;
    width:80%;
`

