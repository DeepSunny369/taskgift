import { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import styled from "styled-components"
import { handleFormValues } from "./formSlice";
import { colors } from "../assets/colors"
import GiftIcon from '../assets/images/giftLogo.png';
import { Button } from "../components/common/Button";
import { AppDispatch, RootState } from "../store";

interface GiftStepOneProps {
    buyingFor?: string,
    age?:string,
    gender?:string,
    hobby?:string,
    currentStep?:string | number
}  

export const GiftStepOne:React.FC = () => {
    const dispatch = useDispatch<AppDispatch>();
    const { form } = useSelector((state: RootState) => state )
    const[stepOneValues, setStepOneValues] = useState<GiftStepOneProps>({
        buyingFor:'',
        age:'',
        gender:'',
        hobby:''
    });
    
    useEffect(() => {
        const {buyingFor = '', age = '', gender = '', hobby= '' } = form;
            setStepOneValues({buyingFor, age,gender,hobby})
    },[])
    
    const handleFormInputs= (e:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const {name, value} = e.target
        setStepOneValues((prev) => ({
            ...prev,
            [name]: value
        }))
    }
    const handleGenderInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target
        setStepOneValues((prev) => ({
            ...prev,
            [name]: value
        }))      
    } 

    const handleBackStep = (e:React.FormEvent) => {
        e.preventDefault();
        alert('Your are on 1st step.')
    }

    const handleNextStep = (e: React.FormEvent) => {
        e.preventDefault();
        let updatedValues:GiftStepOneProps = {...stepOneValues, currentStep: 2};
        dispatch(handleFormValues(updatedValues))
    }


    return(
        <GiftContainer>
           <GiftInnerContainer>
                <GiftLogo>
                    <img  src={GiftIcon}/>
                </GiftLogo>
                <CenterContent>
                    <h1>Tell us about your gift recipient</h1>
                </CenterContent>
            <ContentContainer >
            <InputWrapper>
                <Label>Who are you buying for?</Label>
                <Input 
                    name='buyingFor' 
                    placeholder="Please enter name" 
                    value={stepOneValues?.buyingFor}
                    onChange={handleFormInputs}
                    required/>
            </InputWrapper>
            <InputWrapper>
                <Label>how old are they?</Label>
                <InputWithTxt>
                <Input 
                    name='age' 
                    placeholder="10"
                    width={10} 
                    value={stepOneValues?.age}
                    onChange={handleFormInputs}
                    required
                    />
                years old
                </InputWithTxt>
            </InputWrapper>

            <InputWrapper>
                <Label>They identify as:</Label>
                <CheckBoxContainer>
                    <CheckBox isSelected={stepOneValues?.gender === 'female'}>
                     <Input 
                        type="radio" 
                        name ='gender' 
                        height={15} 
                        width={30} 
                        onChange={handleGenderInput}
                        value='female'
                        checked={stepOneValues?.gender === 'female'}
                        />   
                        <Label>Female</Label>
                    </CheckBox>
                    <CheckBox isSelected={stepOneValues?.gender === 'male'}>
                     <Input 
                        type="radio" 
                        name ='gender' 
                        height={15} 
                        width={30}
                        value='male'
                     onChange={handleGenderInput}
                     isSelected={stepOneValues?.gender === 'male'}
                     />   
                        <Label>Male</Label>
                    </CheckBox>
                    <CheckBox isSelected={stepOneValues?.gender === 'other'}>
                     <Input type="radio" name ='gender' height={15} width={30}
                     onChange={handleGenderInput}
                     value='other'
                     isSelected={stepOneValues?.gender === 'other'}
                     />   
                        <Label>Other</Label>
                    </CheckBox>
                   
                </CheckBoxContainer>
            </InputWrapper>
            <InputWrapper>
                <Label>What do they like to-do? Be specific!</Label>
                <TextArea 
                    name='hobby' 
                    placeholder="Enter interests & hobbies"
                    value={stepOneValues?.hobby}
                    onChange={handleFormInputs}
                    />
            </InputWrapper>
            <ButtonWrapper>
            <Button 
                text="Back" 
                color="87CEEB"
                onClick={handleBackStep}
            />
             <Button 
                text="Continue" 
                color="FFF"
                backgroundColor="#87CEEB"
                onClick={handleNextStep}
            />
            </ButtonWrapper>
            </ContentContainer> 
            </GiftInnerContainer> 
        </GiftContainer>
    )
}

interface inputProps {
    width?:number,
    height?: number,
    isSelected?: boolean
}
export const GiftContainer = styled.div`
    height:auto;
    width: 100%;
    display: flex;
    justify-content:center;
    align-items:center;
    margin: 60px 0px;
`
 
export const GiftInnerContainer = styled.div`
    height: auto;
    width: 50%;
    display:flex;
    justify-content:center;
    flex-direction:column;
    align-items:center;
    
    @media (max-width: 992px) {
        width: 96%;
    }
`

export  const GiftLogo = styled.div`
    display:flex;
    width:100%;
    justify-content:center;

    img {
        background-size:cover;
        background-repeat:no-repeat;
    }
`
export const CenterContent = styled.div`
    display:flex;
    justify-content:center;

    @media (max-width: 500px) {
        h1 {
            font-size:1.2rem;
        }
    }
`
export const InputWrapper = styled.div`
    display:flex;
    flex-direction:column;
    margin: 15px 0px;
`
export const Label = styled.label`
    font-weight:600;
    padding:5px 0px

`
export const Input = styled.input<inputProps>`
    height:${({height}) => height ? height : 40}px;
    width:${({width}) => width ? width : 96}%;
    outline:none;
    border: 2px solid ${colors.grey};
    border-radius:4px;
    padding:0 10px;
`
export const ContentContainer = styled.form`
    height:auto;
    width:70%;
`

const InputWithTxt = styled.div`
    display:flex;
    gap:15px;
    align-items:center;
`

export const CheckBoxContainer = styled.div`
    display:grid;
    grid-template-columns:1fr 1fr 1fr;
    grid-template-rows:1fr;
    gap:10px;
`
const CheckBox = styled.div<inputProps>`
    border:2px solid ${colors.grey};
    border-color:${({isSelected}) => isSelected ? colors.primary : colors.grey};
    display:flex;
    padding: 5px 30px 5px 10px;
    border-radius:5px;
    justify-content:center;
    align-items:center;
`

const TextArea = styled.textarea`
    height:60px;
    border-radius:5px;
    resize:none;
    outline:none;
    padding:10px;
`

export const ButtonWrapper = styled.div`
    display:grid;
    gap:10px;
    grid-template-columns:1fr 1fr;
    grid-template-rows:1fr;
`