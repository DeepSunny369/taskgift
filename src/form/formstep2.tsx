import { useEffect, useState } from "react";
import { ButtonWrapper, CenterContent, CheckBoxContainer, ContentContainer, GiftContainer, GiftInnerContainer, GiftLogo, Input, InputWrapper, Label } from "./formstep1"
import GiftIcon from "../assets/images/giftLogo.png";
import ButtonLogo from '../assets/images/buttonlogo.png';
import { Button } from "../components/common/Button";
import { SelectBox } from "../components/common/SelectInput";
import styled from "styled-components";
import { colors } from "../assets/colors";
import { countryValues } from "../assets/countryjson";
import { useDispatch } from "react-redux";
import { handleFormValues } from "./formSlice";
import { AppDispatch } from "../store";
const giftOptions = [
    { value: 'Birthday', label: 'Birthday' },
    { value: 'Anniversary', label: 'Anniversary' },
    { value: 'Marraige', label: 'Marraige' },
    { value: 'childSpeical', label: 'childSpeical' }
]

interface countryProps {
    value: string,
    label: string,
}
interface GiftStepTwoProps {
    occasion?: string,
    amount?: string,
    country?: string,
    giftType?: string,
    currentStep?: string | number
}



export const GiftStepTwo: React.FC = () => {
    const [countryOptions, setCountryOptions] = useState<countryProps[] | []>([]);
    const [selectedCountry, setSelectedCountry] = useState<countryProps>({ value: '', label: '' });
    const [giftType, setGiftType] = useState<countryProps>({value:'Birthday',label:'Birthday'});
    const dispatch = useDispatch<AppDispatch>();
    const [stepTwoValues, setStepTwoValues] = useState<GiftStepTwoProps>({
        occasion: '',
        amount: '',
        country: '',
        giftType: ''
    });
    useEffect(() => {
        let updatedCountryVal = [];
        updatedCountryVal = countryValues.map((country) => (
            {
                value: country.code,
                label: country.name
            }
        ))
        setCountryOptions(updatedCountryVal);
        setSelectedCountry(updatedCountryVal[0])
    }, [])

    const handleFormInputs = (e:  React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target
        setStepTwoValues((prev) => ({
            ...prev,
            [name]: value
        }))
    }

    const handleNextStep = (e: React.FormEvent) => {
        e.preventDefault();
        const formValues:GiftStepTwoProps = {
            currentStep: 'progress'
        }
        dispatch(handleFormValues(formValues))
    }
    const handleBackStep = (e: React.FormEvent) => {
        e.preventDefault();
        const formValues:GiftStepTwoProps = {
            currentStep: 1
        }
        dispatch(handleFormValues(formValues))
    }
    return (
        <GiftContainer>
            <GiftInnerContainer>
                <GiftLogo>
                    <img src={GiftIcon} />
                </GiftLogo>
                <CenterContent>
                    <h1>Help us to find the right gift!</h1>
                </CenterContent>
                <ContentContainer>
                    <InputWrapper>
                        <Label>What's the occasion?</Label>
                        <Input
                            name='occasion'
                            placeholder="Please enter occasion"
                            value={stepTwoValues.occasion}
                            onChange={handleFormInputs}
                        />
                    </InputWrapper>

                    <InputWrapper>
                        <Label>What type of gift would you like?</Label>
                        <SelectBox
                            options={giftOptions}
                            onChange={(val:countryProps) => setGiftType(val)}
                            value={giftType}
                            name='giftType'
                        />
                    </InputWrapper>

                    <InputWrapper>
                        <Label>what's the maximum you're willing to spend</Label>
                        <SpendContainer>
                            <SpendAmount>USD</SpendAmount>
                            <Input name="amount" width={15} placeholder="50"
                                value={stepTwoValues.amount}
                                onChange={handleFormInputs}
                            />
                        </SpendContainer>
                    </InputWrapper>

                    <InputWrapper>
                        <Label>What country should we return the result for?</Label>
                        <SelectBox
                            options={countryOptions}
                            onChange={(val: countryProps) => setSelectedCountry(val)}
                            value={selectedCountry}
                            name='country'
                        />
                    </InputWrapper>

                    <ButtonWrapper>
                        <Button
                            text="Back"
                            color="87CEEB"
                            onClick={handleBackStep}
                        />
                        <Button
                            text="Generate Gift Ideas"
                            color="FFF"
                            backgroundColor="linear-gradient(90deg, rgba(80,188,217,1) 31%, rgba(241,77,255,1) 71%)"
                            logo={ButtonLogo}
                            onClick={handleNextStep}
                        />
                    </ButtonWrapper>
                </ContentContainer>
            </GiftInnerContainer>
        </GiftContainer>
    )
}
const SpendContainer = styled.div`
    display:flex;
    gap:10px;
    width:100%;
`
const SpendAmount = styled.div`
    height:40px;
    width:15%;
    border:2px solid ${colors.grey};
    display:flex;
    justify-content:center;
    align-items:center;
    border-radius:5px;
`