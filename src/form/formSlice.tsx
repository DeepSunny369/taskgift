import {createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

export interface FormProps  {
    currentStep?: number | string,
    buyingFor?: string,
    age?:string,
    gender?:string,
    hobby?:string 
}

const initialState: FormProps = {
    currentStep: 1,
    buyingFor: '',
    age:'',
    gender:'',
    hobby:''
}

 const formSlice = createSlice({
    name:'form',
    initialState,
    reducers: {
        handleFormValues: (state, action:PayloadAction<FormProps>): any => {
            state = {...state, ...action.payload} 
            return state
        }
    }
});

export const { handleFormValues } = formSlice.actions

export default formSlice.reducer

