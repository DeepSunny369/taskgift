import styled from "styled-components"
import { ProductContainer, ProductWrapper } from "./product"
import Loader from '../assets/images/loading.png';

export const FormLoader = () => {
    return (
        <ProductWrapper>
            <LoaderWrapper>
                <LoaderIcon>
                <img src={Loader} />
                </LoaderIcon>
                <LoaderMainTxt>
                    <h1>
                        Gererating Gifts Ideas...
                    </h1>
                </LoaderMainTxt>
                <LoaderMainTxt>
                    <h4>
                    Hang tight. Our AI-Powered Genie is generating gift ideas. This can take about 5-10 seconds.
                    </h4>
                </LoaderMainTxt>
            </LoaderWrapper>
        </ProductWrapper>
    )
}

const LoaderWrapper = styled.div`
    height:auto;
    width:100%;
    margin-top:100px;
`
const LoaderIcon = styled.div`
    height:auto;
    width:100%;
    display:flex;
    justify-content:center;
    align-items:center;
`
const LoaderMainTxt = styled.div`
    width:100%;
    display:flex;
    justify-content:center;
    align-items:center;
`
