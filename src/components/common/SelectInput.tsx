import Select from 'react-select'

interface selectProps {
    options:any,
    onChange: (e:any) => void,
    value:any,
    name:string
}


export const SelectBox: React.FC<selectProps> = ({options,onChange, value, name}) => {
    return(
       <Select 
        options={options}
        onChange={onChange}
        value={value}
        name={name}
       />
    )
}