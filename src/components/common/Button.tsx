import styled from "styled-components"
import React from 'react';
import { colors } from "../../assets/colors";
import { type } from "os";
interface ButtonProps {
    backgroundColor?: string,
    text?: string,
    children?: React.ReactNode,
    color?:string,
    logo?:any,
    type?:string,
    onClick?: (e:React.FormEvent) => void,
}

export const Button: React.FC<ButtonProps> = ({ text, color, backgroundColor,logo, onClick }) => {
    return (
        <ButtonWrapper backgroundColor={backgroundColor}>
           {
            logo && <ButtonIcon>
            <img src={logo}/>
           </ButtonIcon>
           }
            <ButtonContent
            color={color}
            backgroundColor={backgroundColor}
            onClick={onClick}
            >{text}</ButtonContent>
        </ButtonWrapper>
    )
}


const ButtonWrapper = styled.div<ButtonProps>`
    width:100%;
    height:50px;
    border:2px solid ${colors.grey};
    border-radius:10px;
    display:flex;
    align-items:center;
background:${({ backgroundColor }) => backgroundColor ? backgroundColor : '#FFF'};
`

const ButtonIcon = styled.div`
    width:30px;
    height:30px;
    display:flex;
    justify-content:center;
    align-items:center;
    padding-left: 25px;
    img {
        background-size:cover;
        backgroung-repeat:no-repeat;
        height:100%;
        width:100%;
    }
`

const ButtonContent = styled.button<ButtonProps>`
color:#${({color}) => color ? color : 'FFF'};
width:100%;
outline:none;
height:100%;
border-radius: 10px;
font-size:1.2rem;
font-weight:600;
cursor:pointer;
border:none;
background:transparent;

`
