import styled from "styled-components";
import { colors } from "../../assets/colors";

interface HeaderContainerProps {
    toggleMenu?: boolean
}

export const HeaderContainer = styled.div<HeaderContainerProps>`
    height:'auto';
    background:${colors.grey};
    padding:20px 40px;
    display:grid;
    grid-template-columns: 1fr 3.8fr 1fr;
    grid-template-rows:1fr;

    @media (max-width:800px) {
        grid-template-columns: 1fr 1fr;
    }
`

export const Logo = styled.div`
    height:100%;
    width:100%;
    font-size:2rem;
    display:flex;
    span {
        font-weight:200;
    }
`

export const Menu = styled.div<HeaderContainerProps>`
    height:auto;
    width:100%;
    display:flex;
   @media (max-width:800px) {
    display:none;
   }
`

export const MenuLogo = styled.div`
    height:100%;
    width:100px;
    margin-right:10px;
`
export const MenuContent = styled.div`
    height:auto;
    width:100%;
    img {
        background-size:cover;
        backgroung-repeat:no-repeat;
    }
    display:flex;
    justify-content:center;
    align-items:center;
    font-weight:600;
 @media (max-width: 800px) {
    justify-content: start;
    padding: 10px 0px;
 }

`
export const MenuLogin = styled.div`
    height:auto;
    width:100%;
    display:flex;
    justify-content:center;
    align-items:center;
    @media (max-width:800px) {
        display:none;
    }
`
export const MenuPic = styled.div`
    height:40px;
    width:40px;
    border-radius:50%;
    background:#000;
    margin-left:10px;
`
export const SmallMenu = styled.div`
    height:40px;
    width:100%;
    display:none;
    @media (max-width:800px) {
        display:flex;
        justify-content: end;
    }
`

export const SmallMenuIcon = styled.div`
    height:40px;
    width: 40px;
    background:#000;
    cursor:pointer;
`
export const SmallMenuItems = styled.div<HeaderContainerProps>`
    display: none;
    @media (max-width:800px) {
        display: ${({toggleMenu}) => toggleMenu ? 'flex' : 'none'};
        flex-direction:column;
    }
`