import { HeaderContainer, Logo, Menu, MenuContent, MenuLogin, MenuLogo, MenuPic, SmallMenu, SmallMenuIcon, SmallMenuItems } from "./styledHeader"
import logoPic from '../../assets/images/logo.png';
import { useState } from "react";

export const Header = () => {
    const[toggleMenu, setToggleMenu] = useState<boolean>(false);
    return(
        <HeaderContainer toggleMenu ={toggleMenu}>
            <Logo>
                GIFT
                <span>LIST</span>
            </Logo>
            <Menu>
                <MenuContent><img src={logoPic}/></MenuContent>
                <MenuContent>Lists</MenuContent>
                <MenuContent>GiftExchange</MenuContent>
                <MenuContent>Shop</MenuContent>
                <MenuContent>Occasions</MenuContent>
                <MenuContent>Ecards</MenuContent>
                <MenuContent>Blog</MenuContent>
                <MenuContent>FAQ</MenuContent>
            </Menu>
            <MenuLogin>
                <span>
                jone Smith
                </span>
                <MenuPic></MenuPic>
            </MenuLogin>

            <SmallMenu onClick={() => setToggleMenu((prev) => !prev)}>
                <SmallMenuIcon></SmallMenuIcon>
            </SmallMenu>
            <SmallMenuItems toggleMenu={toggleMenu}>
                <MenuContent><img src={logoPic}/></MenuContent>
                <MenuContent>Lists</MenuContent>
                <MenuContent>GiftExchange</MenuContent>
                <MenuContent>Shop</MenuContent>
                <MenuContent>Occasions</MenuContent>
                <MenuContent>Ecards</MenuContent>
                <MenuContent>Blog</MenuContent>
                <MenuContent>FAQ</MenuContent>
            </SmallMenuItems>
        </HeaderContainer>
    )
}